#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Hexagon :
	public Shape
{
private:
	double _sideLength;

public:
	Hexagon(std::string name, std::string color, double sideLength);
	virtual void draw();
	virtual double CalArea();
	double getSideLength() const;
	void setSideLength(double sideLength);
};

