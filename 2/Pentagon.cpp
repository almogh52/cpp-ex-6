#include "Pentagon.h"

/**
* This is the constructor of Pentagon.
* param name: The name of the pentagon
* param color: The color of the pentagon
* param sideLength: The length of one of the sides of the pentagon
**/
Pentagon::Pentagon(std::string name, std::string color, double sideLength) : Shape(name, color) 
{
	// Set the pentagon side length
	setSideLength(sideLength);
}

/**
* This is the drawing function of the Pentagon, it prints all of it's details
* return: None
**/
void Pentagon::draw()
{
	// Print the color, the name, the side length and the area of the Pentagon
	std::cout << std::endl << "Color is " << getColor() << std::endl;
	std::cout << "Name is " << getName() << std::endl;
	std::cout << "Side length is " << getSideLength() << std::endl;
	std::cout << "Area: " << CalArea() << std::endl;;
}

/**
* This function calcs the area of the pentagon
* return: The area of the pentagon
**/
double Pentagon::CalArea()
{
	// Use the math utils class to calc the pentagon area
	return MathUtils::CalPentagonArea(_sideLength);
}

// Getter
double Pentagon::getSideLength() const
{
	return _sideLength;
}

// Setter
void Pentagon::setSideLength(double sideLength)
{
	// If the given side length is negetive, throw shape exception
	if (sideLength < 0)
	{
		throw shapeException();
	}

	_sideLength = sideLength;
}
