#pragma once
#include <cmath>

#define PENTAGON_AREA_FACTOR 1.72048
#define HEXAGON_AREA_FACTOR 2.59

class MathUtils
{
public:
    static double CalPentagonArea(double side);
    static double CalHexagonArea(double side);
};

