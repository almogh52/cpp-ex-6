#include "MathUtils.h"

/**
* This functions calculates the area of a Pentagon
* param side: The side length of the Pentagon
* return: The area of the Pentagon
**/
double MathUtils::CalPentagonArea(double side)
{
    // Calc the pentagon area using it's formula
    return pow(side, 2) * PENTAGON_AREA_FACTOR;
}

/**
* This functions calculates the area of a Hexagon
* param side: The side length of the Hexagon
* return: The area of the Hexagon
**/
double MathUtils::CalHexagonArea(double side)
{
    // Calc the hexagon area using it's formula
    return pow(side, 2) * HEXAGON_AREA_FACTOR;
}
