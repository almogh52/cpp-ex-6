#include "InputException.h"

/**
* This function overrides the what function of std::exception and returns what the exception is
* return: The exception detail
**/
const char * InputException::what() const
{
	return "You've entered invalid input!";
}
