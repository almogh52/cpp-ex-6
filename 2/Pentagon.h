#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Pentagon :
	public Shape
{
private:
	double _sideLength;

public:
	Pentagon(std::string name, std::string color, double sideLength);
	virtual void draw();
	virtual double CalArea();
	double getSideLength() const;
	void setSideLength(double sideLength);
};

