#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"
#include "Hexagon.h"
#include "Pentagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0; double sideLength = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Hexagon hex(nam, col, sideLength);
	Pentagon penta(nam, col, sideLength);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrHex = &hex;
	Shape *ptrPenta = &penta;
	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; std::string shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, z = pentagon" << std::endl;
		
		// Get the shape type as a string in order to leave an empty buffer
		std::cin >> shapetype;

		// If the shapetype string is longer than 1 (more than 1 shape is needed), send warning
		if (shapetype.length() != 1)
		{
			// Print warning
			std::cout << "WARNING - Don't try to build more than one shape at once!" << std::endl;
		}

	try
		{
			// Switch on the first character in the shapetype (ignore the other shape types)
			switch (shapetype[0]) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;

				// If the stream input failed to insert the corrent types to the variables, throw an input exception
				if (std::cin.fail())
				{
					throw InputException();
				}

				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;

				// If the stream input failed to insert the corrent types to the variables, throw an input exception
				if (std::cin.fail())
				{
					throw InputException();
				}

				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;

				// If the stream input failed to insert the corrent types to the variables, throw an input exception
				if (std::cin.fail())
				{
					throw InputException();
				}

				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;

				// If the stream input failed to insert the corrent types to the variables, throw an input exception
				if (std::cin.fail())
				{
					throw InputException();
				}

				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();

			case 'h':
				// Get the name of the hexagon
				std::cout << "Enter the name of the Hexagon: ";
				std::cin >> nam;

				// Get the color of the hexagon
				std::cout << "Enter the color of the Hexagon: ";
				std::cin >> col;

				// Get the side length of the hexagon
				std::cout << "Enter the side length of the Hexagon: ";
				std::cin >> sideLength;

				// If the stream input failed to insert the corrent types to the variables, throw an input exception
				if (std::cin.fail())
				{
					throw InputException();
				}

				// Set the Hexagon's name, color and side length
				hex.setName(nam);
				hex.setColor(col);
				hex.setSideLength(sideLength);

				// Draw the Hexagon
				ptrHex->draw();

				break;

			case 'z':
				// Get the name of the pentagon
				std::cout << "Enter the name of the Pentagon: ";
				std::cin >> nam;

				// Get the color of the pentagon
				std::cout << "Enter the color of the Pentagon: ";
				std::cin >> col;

				// Get the side length of the pentagon
				std::cout << "Enter the side length of the Pentagon: ";
				std::cin >> sideLength;

				// If the stream input failed to insert the corrent types to the variables, throw an input exception
				if (std::cin.fail())
				{
					throw InputException();
				}

				// Set the Pentagon's name, color and side length
				penta.setName(nam);
				penta.setColor(col);
				penta.setSideLength(sideLength);

				// Draw the Pentagon
				ptrPenta->draw();

				break;

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get(x); // Get the newline and ignore it
			std::cin.get(x); // Get the actual character for x variable
		}
		catch (InputException& e)
		{
			// Clear the failure state of the stream
			std::cin.clear();

			// Tell the stream to ignore all the characters in it until it encounters new line
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

			// Print the exception error
			std::cout << e.what() << std::endl;
		}
		catch (std::exception& e)
		{			
			// Print the exception error
			std::cout << e.what() << std::endl;
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("pause");
	return 0;
}