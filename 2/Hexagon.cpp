#include "Hexagon.h"

/**
* This is the constructor of Hexagon.
* param name: The name of the hexagon
* param color: The color of the hexagon
* param sideLength: The length of one of the sides of the hexagon
**/
Hexagon::Hexagon(std::string name, std::string color, double sideLength) : Shape(name, color) 
{
	// Set the hexagon side length
	setSideLength(sideLength);
}

/**
* This is the drawing function of the Hexagon, it prints all of it's details
* return: None
**/
void Hexagon::draw()
{
	// Print the color, the name, the side length and the area of the Hexagon
	std::cout << std::endl << "Color is " << getColor() << std::endl;
	std::cout << "Name is " << getName() << std::endl;
	std::cout << "Side length is " << getSideLength() << std::endl;
	std::cout << "Area: " << CalArea() << std::endl;;
}

/**
* This function calcs the area of the hexagon
* return: The area of the hexagon
**/
double Hexagon::CalArea()
{
	// Use the math utils class to calc the hexagon area
	return MathUtils::CalHexagonArea(_sideLength);
}

// Getter
double Hexagon::getSideLength() const
{
	return _sideLength;
}

// Setter
void Hexagon::setSideLength(double sideLength)
{
	// If the given side length is negetive, throw shape exception
	if (sideLength < 0)
	{
		throw shapeException();
	}

	_sideLength = sideLength;
}
