#include <iostream>
#include <string>

#define FORBIDDEN_NUM 8200

#define EXIT_CHOICE 4

int add(int a, int b) {
	// If the sum of both numbers is the forbidden number throw error
	if (a + b == FORBIDDEN_NUM)
	{
		throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year!");
	}

	return a + b;
}

int multiply(int a, int b) {
	int sum = 0;

	// Perform the multiply of a in b by adding to the sum a, b times, if the sum is 8200, the add function will throw an error, that will be catched in the main
	for(int i = 0; i < b; i++) {
		// This function will throw an error if the sum is 8200
		sum = add(sum, a);
	};

	return sum;
}

int pow(int a, int b) {
	int exponent = 1;

	// Perform the power of a in b by multiply a by itself, b times, the multiply function will throw an error if the exponent is 8200
	for(int i = 0; i < b; i++) {
		// This function will throw 8200 if the multiply result is 8200
		exponent = multiply(exponent, a);
	};

	return exponent;
}

int main(void) {
	int(*mathFuncs[])(int a, int b) = { add, multiply, pow };
	int choice = 0, a = 0, b = 0, result = 0;

	// While the user didn't chose to leave, continue printing menu
	do {
		// Clear the screen
		system("cls");

		// Print calculator's menu
		std::cout << "Magshimim's Calculator:" << std::endl;
		std::cout << "1. Add function" << std::endl;
		std::cout << "2. Multiply function" << std::endl;
		std::cout << "3. Power function" << std::endl;
		std::cout << "4. Exit" << std::endl;

		// Get a choice from the user
		std::cin >> choice;

		// If the user chose invalid choice or chose to exit, skip this loop iteration
		if (choice < 1 || choice >= EXIT_CHOICE)
		{
			continue;
		}

		// Get from the user the 2 numbers
		std::cout << "Please the first number: ";
		std::cin >> a;

		std::cout << "Please the second number: ";
		std::cin >> b;

		// Call the math function, if the error code was success, print the result
		try {
			// Try to print the result of the math function
			std::cout << std::endl << "The result is: " << mathFuncs[choice - 1](a, b) << std::endl;
		}
		catch (std::string error)
		{
			// If an error was thrown, catch it and print it
			std::cout << error << std::endl;
		}

		// Wait for key press
		system("pause");
	} while (choice != EXIT_CHOICE);

	system("pause");

	return 0;
}