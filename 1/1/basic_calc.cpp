#include <iostream>

#define FORBIDDEN_NUM 8200

#define CALC_SUCCESS 0
#define CALC_FAILURE 1

#define EXIT_CHOICE 4

int add(int a, int b, int& result) {
	int errorCode = CALC_SUCCESS;

	// If the sum of both numbers is the forbidden number return failure
	if (a + b == FORBIDDEN_NUM)
	{
		// Set the error code as failure and print the error
		errorCode = CALC_FAILURE;
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year!" << std::endl;
	}
	else {
		// Calc the result of the sum of the first number to the second number and return it using the result ref
		result = a + b;
	}

	return errorCode;
}

int multiply(int a, int b, int& result) {
	int errorCode = CALC_SUCCESS;
	int sum = 0;

	// Calc the sum
	for (int i = 0; i < b; i++) {
		// Add to the sum a and store it in the sum var, if it was a failure set this function as a failure as well
		if (add(sum, a, sum) == CALC_FAILURE)
		{
			// Set error code as failure and break, no need to print error since it's already printing it in the add function
			errorCode = CALC_FAILURE;
			break;
		}
	}

	// If the error code is success, set the result
	if (errorCode == CALC_SUCCESS)
	{
		// Return the sum using the result ref
		result = sum;
	}

	return errorCode;
}

int pow(int a, int b, int& result) {
	int errorCode = CALC_SUCCESS;
	int exponent = 1;

	// Calc the power of a in b
	for (int i = 0; i < b; i++) {
		// Multiply the current exponent in a and store the result in exponent, if it returned failure or the current exponent is forbidden, send error
		if (multiply(exponent, a, exponent) == CALC_FAILURE || exponent == FORBIDDEN_NUM)
		{
			// Set error code as failure and break, no need to print error since it's already printing it in the add function
			errorCode = CALC_FAILURE;
			break;
		}
	};

	// If the error code is success, set the result
	if (errorCode == CALC_SUCCESS)
	{
		// Return the power using the result ref var
		result = exponent;
	}

	return errorCode;
}

int main(void) {
	int(*mathFuncs[])(int a, int b, int& result) = { add, multiply, pow };
	int choice = 0, a = 0, b = 0, result = 0;

	// While the user didn't chose to leave, continue printing menu
	do {
		// Clear the screen
		system("cls");

		// Print calculator's menu
		std::cout << "Magshimim's Calculator:" << std::endl;
		std::cout << "1. Add function" << std::endl;
		std::cout << "2. Multiply function" << std::endl;
		std::cout << "3. Power function" << std::endl;
		std::cout << "4. Exit" << std::endl;

		// Get a choice from the user
		std::cin >> choice;

		// If the user chose invalid choice or chose to exit, skip this loop iteration
		if (choice < 1 || choice >= EXIT_CHOICE)
		{
			continue;
		}

		// Get from the user the 2 numbers
		std::cout << "Please the first number: ";
		std::cin >> a;

		std::cout << "Please the second number: ";
		std::cin >> b;

		// Call the math function, if the error code was success, print the result
		if (mathFuncs[choice - 1](a, b, result) == CALC_SUCCESS)
		{
			std::cout << std::endl << "The result is: " << result << std::endl;
		}

		// Wait for key press
		system("pause");
	} while (choice != EXIT_CHOICE);

	system("pause");

	return 0;
}